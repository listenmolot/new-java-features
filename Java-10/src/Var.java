import java.util.HashMap;
import java.util.Map;

public class Var {
//	var p;
	
	//illegal
	public var = "hello"; // error: 'var' is not allowed here

	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		var message = "Hello, Java 10";
		System.out.println(message instanceof String);
		
		//without
		Map<Integer, String> map = new HashMap<>();
		//with
		var idToNameMap = new HashMap<Integer, String>();
		
		
	}
	
//	static var a(var i) {
//		
//	}
	
	static void var() {
		var var = 10;
	}
	static void illegal() {
		var n; // error: cannot use 'var' on variable without initializer

		var emptyList = null; // error: variable initializer is 'null'

		var p = (String s) -> s.length() > 10; // error: lambda expression needs an explicit target-type

		var arr = { 1, 2, 3 }; // error: array initializer needs an explicit target-type -> new int[]{ 1, 2, 3 }
	}
	
	static void guide() {
		var result = process();
		System.out.println(result instanceof String);
	}
	
	static String process() {
		
	}

}
