import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CopingList {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(3);
		list.add(5);
		list.add(7);
		
		byContstructor(list);
		addAll(list);
		collectionsCopy(list);
		byStreams(list);
		byVersion10(list);
	}
	
	static void byContstructor(List<Integer> list) {
		List<Integer> copy = new ArrayList<>(list);
		System.out.println(copy);
	}
	
	static void addAll(List<Integer> list) {
		List<Integer> copy = new ArrayList<>();
		copy.addAll(list);
		System.out.println(copy);
	}
	
	static void collectionsCopy(List<Integer> source) {
		List<Integer> dest1 = Arrays.asList(10, 11, 12, 13);
		Collections.copy(dest1, source);
		System.out.println(dest1);
		
		List<Integer> dest2 = Arrays.asList(10, 11, 12, 14, 15);
		Collections.copy(dest2, source);
		System.out.println(dest2);
		
//		List<Integer> dest3 = Arrays.asList(10, 11, 12);
//		Collections.copy(dest3, source);
//		System.out.println(dest3);
	}
	
	static void byStreams(List<Integer> list) {
		List<Integer> copy1 = list.stream()
				  .collect(Collectors.toList());
		System.out.println("copy1: "+copy1);
		
		List<Integer> copy2 = list.stream()
				  .skip(1)
				  .collect(Collectors.toList());
		System.out.println("copy2: "+copy2);
		
		List<Integer> copy3 = list.stream()
				  .filter(v -> v < 5)
				  .collect(Collectors.toList());
		System.out.println("copy3: "+copy3);
		
		List<Integer> nullList = null;
		List<Integer> copy4 = Optional.ofNullable(nullList)
				.map(List::stream)
				.orElseGet(Stream::empty)
				.collect(Collectors.toList());
		System.out.println("copy4: "+copy4);
		
		List<Integer> emptyList = new ArrayList<Integer>();
		List<Integer> copy5 = Optional.ofNullable(emptyList)
				.map(List::stream)
				.orElseGet(Stream::empty)
				.collect(Collectors.toList());
		System.out.println("copy5: "+copy5);
		
		List<Integer> copy6 = Optional.ofNullable(list)
				.map(List::stream)
				.orElseGet(Stream::empty)
				.skip(1)
				.filter(v->v<7)
				.collect(Collectors.toList());
		System.out.println("copy6: "+copy6);
	}
	
	static void byVersion10(List<Integer> list) {
		List<Integer> copy = List.copyOf(list);
		System.out.println("byVersion10: "+copy);
	}


}
