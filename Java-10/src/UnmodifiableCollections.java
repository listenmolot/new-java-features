import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UnmodifiableCollections {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Integer> list = new ArrayList<Integer>();
		list.add(1);
		list.add(3);
		list.add(5);
		list.add(7);
//		copyOf(list);
		toUnmodifiable(list);
		orElseThrow(list);
	}
	
	static void copyOf(List<Integer> list) {
		List<Integer> copyList = List.copyOf(list);
	    copyList.add(9);
	}
	
	static void toUnmodifiable(List<Integer> list) {
		List<Integer> evenList = list.stream()
			      .filter(i -> i % 2 == 0)
			      .collect(Collectors.toUnmodifiableList());
		evenList.add(9);
	}
	
	static void orElseThrow(List<Integer> list) {
		Integer firstEven = list.stream()
			      .filter(i -> i % 2 == 0)
			      .findFirst()
			      .orElseThrow();
		System.out.println(firstEven.equals(Integer.valueOf(2)));
	}

}
