
//https://howtodoinjava.com/java7/improved-exception-handling/
public class ExceptionHandlingEnhancement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

//		Handling More Than One Type of Exception
		handlingMoreThanOne();

//		Rethrowing Exceptions with More Inclusive Type Checking
		try {
			rethrowException1("First");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			rethrowException2("");
		} catch (FirstException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecondException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		sampleMethod();
	}

	static void handlingMoreThanOne() {
		try {
			// Do some processing which throws NullPointerException;
			throw new NullPointerException();
		}
		// You can catch multiple exception added after 'pipe' character
		catch (NullPointerException | IndexOutOfBoundsException ex) {
			throw ex;
		}
	}

	public static void rethrowException1(String exceptionName) throws Exception {
		try {
			if (exceptionName.equals("First")) {
				throw new FirstException();
			} else {
				throw new SecondException();
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public static void rethrowException2(String exceptionName) throws FirstException, SecondException {
		try {
			if (exceptionName.equals("First")) {
				throw new FirstException();
			} else {
				throw new SecondException2();
			}
		} catch (Exception e) {
			throw e;
		}
	}

	public static void sampleMethod()
	// throws Throwable //No need to do this
	{
		try {
			//Do some processing which throws NullPointerException; I am sending directly
			throw new NullPointerException();
		}
		//You can catch multiple exception added after 'pipe' character
		catch (NullPointerException | IndexOutOfBoundsException ex) {
			throw ex;
		}
		//Now method sampleMethod() do not need to have 'throws' clause
		catch (Throwable ex) {
			throw ex;
		}
	}

	@SuppressWarnings("serial")
	static class FirstException extends Exception {
	}

	@SuppressWarnings("serial")
	static class SecondException extends Exception {
	}

	@SuppressWarnings("serial")
	static class SecondException2 extends SecondException {
	}

}
