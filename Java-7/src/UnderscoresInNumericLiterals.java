
//https://howtodoinjava.com/java7/improved-formatted-numbers-in-java-7/
public class UnderscoresInNumericLiterals {

	@SuppressWarnings("unused")
	public static void main(String[] args)
	  {
	    /**
	     * Supported in int
	     * */
	    int improvedInt = 10______00_000;
	 
	    /**
	     * Supported in float
	     * */
	    float improvedFloat = 10_00_000f;
	 
	    /**
	     * Supported in long
	     * */
	    float improvedLong = 10_00_000l;
	 
	    /**
	     * Supported in double
	     * */
	    float improvedDouble = 10_00_000;
	    
	    //cannot
//	    float a = 10_00_000_;
//	    float b = _10_00_000_;
	  }

}
