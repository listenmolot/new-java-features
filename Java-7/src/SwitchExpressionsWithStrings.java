
//https://howtodoinjava.com/java7/improved-type-inference-in-java-7/
	
public class SwitchExpressionsWithStrings {

	public static void main(String[] args) {
		System.out.println(getExpendedMessage1("one"));
		System.out.println(getExpendedMessage1("three"));
		System.out.println(getExpendedMessage1("five"));

		System.out.println(getExpendedMessage2("one"));
		System.out.println(getExpendedMessage2("two"));
	}

	static String getExpendedMessage1(final String token) {
		String value = null;

		switch (token) {
		case ("one"):
			value = "Token one identified";
			break;

		case ("two"):
			value = "Token two identified";
			break;

		case ("three"):
			value = "Token three identified";
			break;

		case ("four"):
			value = "Token four identified";
			break;

		default:
			value = "No token was identified";
		}

		return value;
	}

	static String getExpendedMessage2(final String token) {
		String value = null;

		switch (token) {
		case ("one"):
		case ("three"):
			value = "Odd token identified";
			break;

		case ("two"):
		case ("four"):
			value = "Even token identified";
			break;

		default:
			value = "No token was identified";
		}

		return value;
	}

}
