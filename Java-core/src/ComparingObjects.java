import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

//https://www.baeldung.com/java-comparing-objects

public class ComparingObjects {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		operatorsPrimitives();
		operatorsObjects();
		objectEquals();
		objectsEquals();
		comparable();
		comaparator();
		useOfComaparatorMethods();
	}
	
	static void operatorsPrimitives() {
		System.out.println(1==1);
		
		Integer a = Integer.valueOf(1);
		System.out.println(1==a);
	}
	
	static void operatorsObjects() {
		//By comparing two objects, the value of those objects is not 1. 
		//Rather it is their memory addresses in the stack that are different since both objects were created using the new operator
		Integer a = new Integer(1);
		Integer b = new Integer(1);
		
		System.out.println(a==b);
		
		//but
		Integer a1 = new Integer(1);
		Integer b1 = a1;
		
		System.out.println(a1==b1);
		
//		In this case, they are considered the same. This is because the valueOf() method stores the Integer 
//		in a cache to avoid creating too many wrapper objects with the same value. 
//		Therefore, the method returns the same Integer instance for both calls.
		Integer a2 = Integer.valueOf(1);
		Integer b2 = Integer.valueOf(1);
		
		System.out.println(a2==b2);
		
		// the same with String
		System.out.println("Hello!" == "Hello!");
		
		String a3 = new String("Hello!");
		String b3 = new String("Hello!");
		System.out.println(a3==b3);
		
		System.out.println(null == null);
		System.out.println("Hello!" == null);
	}
	
	static void objectEquals() {
		System.out.println("--objectEquals");
		Integer a = new Integer(1);
		Integer b = new Integer(1);
		
		System.out.println(a.equals(b));
		
		Person1 p1 = new Person1("1", "2");
		Person1 p2 = new Person1("1", "2");
		System.out.println(p1.equals(p2));
		
		Person2 p3 = new Person2("1", "2", "3");
		Person2 p4 = new Person2("1", "2", "3");
		System.out.println(p3.equals(p4));
	}
	
	static void objectsEquals() {
		System.out.println("--objectsEquals");
		Person2 joe = new Person2("Joe", "Portman", "3");
		Person2 joeAgain = new Person2("Joe", "Portman", "3");
		Person2 joeAgain2 = new Person2("Joe", "Portman", null);
		Person2 natalie = new Person2("Natalie", "Portman", "3");

		System.out.println(Objects.equals(joe, joeAgain));
		System.out.println(Objects.equals(joeAgain, joeAgain2));
		System.out.println(Objects.equals(joe, natalie));
		System.out.println(Objects.equals(null, null));
		System.out.println(Objects.equals(null, joe));
		System.out.println(Objects.equals(joe, null));
	}
	
	static void comparable() {
		System.out.println("--comparable");
		Person1 joe = new Person1("Joe", "Portman");
		Person1 allan = new Person1("Allan", "Dale");
		List<Person1> people = new ArrayList<>();
		people.add(joe);
		people.add(allan);
		System.out.println(people);
		Collections.sort(people);
		System.out.println(people);
	}
	
	static void comaparator() {
		System.out.println("--comaparator");
		Comparator<Person2> compareByFirstNames = Comparator.comparing(Person2::getFirstName);
		Person2 joe = new Person2("Joe", "Portman", "2");
		Person2 allan = new Person2("Allan", "Dale", "1");

		List<Person2> people = new ArrayList<>();
		people.add(joe);
		people.add(allan);

		System.out.println(people);
		people.sort(compareByFirstNames);
		System.out.println(people);
	}
	
	static void useOfComaparatorMethods() {
		System.out.println("--useOfComaparatorMethods");
		Person2 joe1 = new Person2("Joe", "Portman", "3");
		Person2 joe2 = new Person2("Joe", "Portman", "1");
		Person2 joe3 = new Person2("Joe", "Portman", null);
		Person2 joe4 = new Person2("Joe", "Portman", "2");
		Person2 allan = new Person2("Allan", "Dale", "1");

		List<Person2> people = new ArrayList<>();
		people.add(joe1);
		people.add(joe2);
		people.add(joe3);
		people.add(joe4);
		people.add(allan);

		System.out.println(people);
		Collections.sort(people);
		System.out.println(people);
	}

}

class Person1 implements Comparable<Person1>{
    private String firstName;
    private String lastName;

    public Person1(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        
    }
    
    @Override
    public int compareTo(Person1 o) {
        return this.lastName.compareTo(o.lastName);
    }

	@Override
	public String toString() {
		return "Person1 [firstName=" + firstName + ", lastName=" + lastName + "]";
	}
    
    
}

class Person2 implements Comparable<Person2>{
    private String firstName;
    private String lastName;
    private String birthDate;

    public Person2(String firstName, String lastName, String birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person2 that = (Person2) o;
        return firstName.equals(that.firstName) &&
          lastName.equals(that.lastName) &&
          Objects.deepEquals(birthDate, that.birthDate);
    }

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	@Override
	public String toString() {
		return "Person2 [firstName=" + firstName + ", lastName=" + lastName + ", birthDate=" + birthDate + "]\n";
	}
	
	@Override
	public int compareTo(Person2 o) {
	    return Comparator.comparing(Person2::getLastName)
	      .thenComparing(Person2::getFirstName)
	      .thenComparing(Person2::getBirthDate, Comparator.nullsLast(Comparator.naturalOrder()))
	      .compare(this, o);
	}
    
    
}
