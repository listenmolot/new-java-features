package sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

//https://www.baeldung.com/java-sorting

public class SortingInJava {

	static int[] toSort;
	static int[] sortedInts;
	static int[] sortedRangeInts;
	static Employee[] employees;
	static Employee[] employeesSorted;
	static Employee[] employeesSortedByAge;
	static HashMap<Integer, String> map;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		initVariables();
		givenIntArray_whenUsingSort_thenSortedArray();
		givenIntArray_whenUsingRangeSort_thenRangeSortedArray();
		givenIntArray_whenUsingParallelSort_thenArraySorted();
		givenIntegerArray_whenUsingSort_thenSortedArray();
		givenList_whenUsingSort_thenSortedList();
		givenSet_whenUsingSort_thenSortedSet();
		givenMap_whenSortingByKeys_thenSortedMap_1();
		givenMap_whenSortingByKeys_thenSortedMap_2();
		givenMap_whenSortingByValues_thenSortedMap_1();
		givenMap_whenSortingByValues_thenSortedMap_2();
		givenEmpArray_SortEmpArray_thenSortedArrayinNaturalOrder();
		givenIntegerArray_whenUsingSort_thenSortedArray_2();
		givenArray_whenUsingSortWithLambdas_thenSortedArray();
		givenArrayObjects_whenUsingComparing_thenSortedArrayObjects();
	}

	static void initVariables() {

		toSort = new int[] { 5, 1, 89, 255, 7, 88, 200, 123, 66 };
		sortedInts = new int[] { 1, 5, 7, 66, 88, 89, 123, 200, 255 };
		sortedRangeInts = new int[] { 5, 1, 89, 7, 88, 200, 255, 123, 66 };

		employees = new Employee[] { new Employee("John", 23, 5000), new Employee("Steve", 26, 6000),
				new Employee("Frank", 33, 7000), new Employee("Earl", 43, 10000), new Employee("Jessica", 23, 4000),
				new Employee("Pearl", 33, 6000) };
		employeesSorted = new Employee[] { new Employee("Earl", 43, 10000), new Employee("Frank", 33, 70000),
				new Employee("Jessica", 23, 4000), new Employee("John", 23, 5000), new Employee("Pearl", 33, 4000),
				new Employee("Steve", 26, 6000) };
		employeesSortedByAge = new Employee[] { new Employee("John", 23, 5000), new Employee("Jessica", 23, 4000),
				new Employee("Steve", 26, 6000), new Employee("Frank", 33, 70000), new Employee("Pearl", 33, 4000),
				new Employee("Earl", 43, 10000) };

		map = new HashMap<>();
		map.put(55, "John");
		map.put(22, "Apple");
		map.put(66, "Earl");
		map.put(77, "Pearl");
		map.put(12, "George");
		map.put(6, "Rocky");

	}

	static void givenIntArray_whenUsingSort_thenSortedArray() {
		System.out.println("--givenIntArray_whenUsingSort_thenSortedArray");
		int[] toSort2 = Arrays.copyOf(toSort, toSort.length);
		Arrays.sort(toSort2);
		System.out.println(Arrays.equals(toSort2, sortedInts));
		System.out.println(Arrays.toString(toSort2));
	}

	static void givenIntArray_whenUsingRangeSort_thenRangeSortedArray() {
		System.out.println("--givenIntArray_whenUsingSort_thenSortedArray");
		int[] toSort2 = Arrays.copyOf(toSort, toSort.length);
		Arrays.sort(toSort2, 3, 7);

		System.out.println(Arrays.equals(toSort2, sortedRangeInts));
		System.out.println(Arrays.toString(toSort2));
		System.out.println(Arrays.toString(sortedRangeInts));
	}

	static void givenIntArray_whenUsingParallelSort_thenArraySorted() {
		System.out.println("--givenIntArray_whenUsingParallelSort_thenArraySorted");
		int[] toSort2 = Arrays.copyOf(toSort, toSort.length);
		Arrays.parallelSort(toSort2);

		System.out.println(Arrays.equals(toSort2, sortedInts));
		System.out.println(Arrays.toString(toSort2));
	}

	static void givenIntegerArray_whenUsingSort_thenSortedArray() {
		System.out.println("--givenIntegerArray_whenUsingSort_thenSortedArray");
		Integer[] integers = Arrays.stream(toSort).boxed().toArray(Integer[]::new);
		Integer[] sortedIntegers = Arrays.stream(sortedInts).boxed().toArray(Integer[]::new);

		Arrays.sort(integers, Comparator.comparingInt(a -> a));

		System.out.println(Arrays.equals(integers, sortedIntegers));
		System.out.println(Arrays.toString(integers));

		// or
		integers = Arrays.stream(toSort).boxed().toArray(Integer[]::new);
		sortedIntegers = Arrays.stream(sortedInts).boxed().toArray(Integer[]::new);

		Arrays.sort(integers, new Comparator<Integer>() {
			@Override
			public int compare(Integer a, Integer b) {
				return Integer.compare(a, b);
			}
		});
		System.out.println(Arrays.equals(integers, sortedIntegers));
		System.out.println(Arrays.toString(integers));
	}

	static void givenList_whenUsingSort_thenSortedList() {
		System.out.println("--givenList_whenUsingSort_thenSortedList");
		List<Integer> toSortList = Arrays.stream(toSort).boxed().collect(Collectors.toList());
		List<Integer> sortedIntegers = Arrays.stream(sortedInts).boxed().collect(Collectors.toList());

		System.out.println(toSortList);
		Collections.sort(toSortList);

		System.out.println(Arrays.equals(toSortList.toArray(), sortedIntegers.toArray()));
		System.out.println(toSortList);
	}

	static void givenSet_whenUsingSort_thenSortedSet() {
		System.out.println("--givenSet_whenUsingSort_thenSortedSet");
		HashSet<Integer> integersSet = new LinkedHashSet<>(Arrays.stream(toSort).boxed().collect(Collectors.toSet()));
		HashSet<Integer> descSortedIntegersSet = new LinkedHashSet<>(Arrays.asList(255, 200, 123, 89, 88, 66, 7, 5, 1));

		ArrayList<Integer> list = new ArrayList<>(integersSet);
		list.sort(Comparator.reverseOrder());
		integersSet = new LinkedHashSet<>(list);

		System.out.println(Arrays.equals(integersSet.toArray(), descSortedIntegersSet.toArray()));
	}

	static void givenMap_whenSortingByKeys_thenSortedMap_1() {
		System.out.println("--givenMap_whenSortingByKeys_thenSortedMap_1");
		Integer[] sortedKeys = new Integer[] { 6, 12, 22, 55, 66, 77 };

		List<Map.Entry<Integer, String>> entries = new ArrayList<>(map.entrySet());
		entries.sort(Comparator.comparing(Entry::getKey));
		HashMap<Integer, String> sortedMap = new LinkedHashMap<>();
		for (Map.Entry<Integer, String> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		System.out.println((Arrays.equals(sortedMap.keySet().toArray(), sortedKeys)));
	}

	static void givenMap_whenSortingByKeys_thenSortedMap_2() {
		System.out.println("--givenMap_whenSortingByKeys_thenSortedMap_2");
		Integer[] sortedKeys = new Integer[] { 6, 12, 22, 55, 66, 77 };

		List<Map.Entry<Integer, String>> entries = new ArrayList<>(map.entrySet());
		Collections.sort(entries, new Comparator<Entry<Integer, String>>() {
			@Override
			public int compare(Entry<Integer, String> o1, Entry<Integer, String> o2) {
				return o1.getKey().compareTo(o2.getKey());
			}
		});
		Map<Integer, String> sortedMap = new LinkedHashMap<>();
		for (Map.Entry<Integer, String> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		System.out.println((Arrays.equals(sortedMap.keySet().toArray(), sortedKeys)));
	}

	static void givenMap_whenSortingByValues_thenSortedMap_1() {
		System.out.println("--givenMap_whenSortingByValues_thenSortedMap");
		String[] sortedValues = new String[] { "Apple", "Earl", "George", "John", "Pearl", "Rocky" };

		List<Map.Entry<Integer, String>> entries = new ArrayList<>(map.entrySet());
		entries.sort(Comparator.comparing(Entry::getValue));
		HashMap<Integer, String> sortedMap = new LinkedHashMap<>();
		for (Map.Entry<Integer, String> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		System.out.println(Arrays.equals(sortedMap.values().toArray(), sortedValues));
	}

	static void givenMap_whenSortingByValues_thenSortedMap_2() {
		System.out.println("--givenMap_whenSortingByValues_thenSortedMap_2");
		String[] sortedValues = new String[] { "Apple", "Earl", "George", "John", "Pearl", "Rocky" };

		List<Map.Entry<Integer, String>> entries = new ArrayList<>(map.entrySet());
		Collections.sort(entries, new Comparator<Entry<Integer, String>>() {
			@Override
			public int compare(Entry<Integer, String> o1, Entry<Integer, String> o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		});
		Map<Integer, String> sortedMap = new LinkedHashMap<>();
		for (Map.Entry<Integer, String> entry : entries) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		System.out.println(Arrays.equals(sortedMap.values().toArray(), sortedValues));
	}

	static void givenEmpArray_SortEmpArray_thenSortedArrayinNaturalOrder() {
		System.out.println("--givenEmpArray_SortEmpArray_thenSortedArrayinNaturalOrder");
		Arrays.sort(employees);

		System.out.println(Arrays.equals(employees, employeesSorted));
	}

	static void givenIntegerArray_whenUsingSort_thenSortedArray_2() {
		System.out.println("--givenIntegerArray_whenUsingSort_thenSortedArray_2");
		Integer[] integers = Arrays.stream(toSort).boxed().toArray(Integer[]::new);
		Integer[] sortedIntegers = Arrays.stream(sortedInts).boxed().toArray(Integer[]::new);
		Arrays.sort(integers, new Comparator<Integer>() {
			@Override
			public int compare(Integer a, Integer b) {
				return Integer.compare(a, b);
			}
		});

		System.out.println(Arrays.equals(integers, sortedIntegers));

		System.out.println(Arrays.toString(employees));
		Arrays.sort(employees, new Comparator<Employee>() {
			@Override
			public int compare(Employee o1, Employee o2) {
				return Double.compare(o1.getSalary(), o2.getSalary());
			}
		});
		System.out.println(Arrays.toString(employees));
	}

	static void givenArray_whenUsingSortWithLambdas_thenSortedArray() {
		System.out.println("--givenArray_whenUsingSortWithLambdas_thenSortedArray");
		Integer[] integersToSort = Arrays.stream(toSort).boxed().toArray(Integer[]::new);
		Integer[] sortedIntegers = Arrays.stream(sortedInts).boxed().toArray(Integer[]::new);

		// instead
//		Comparator<Integer> c  = new Comparator<>() {
//		    @Override
//		    public int compare(Integer a, Integer b) {
//		        return Integer.compare(a, b);
//		    }
//		};
//		Arrays.sort(integersToSort, c);

		// use lambda
		Arrays.sort(integersToSort, (a, b) -> {
			return Integer.compare(a, b);
		});

		System.out.println(Arrays.equals(integersToSort, sortedIntegers));
		System.out.println(Arrays.asList(integersToSort));
	}

	static void givenArrayObjects_whenUsingComparing_thenSortedArrayObjects() {
		System.out.println("--givenArrayObjects_whenUsingComparing_thenSortedArrayObjects");
		List<Employee> employeesList = Arrays.asList(employees);

        employeesList.sort(Comparator.comparing(Employee::getAge));// .thenComparing(Employee::getName));

        System.out.println(Arrays.equals(employeesList.toArray(), employeesSortedByAge));
        System.out.println(employeesList);
        
        employeesList.sort(Comparator.comparing(Employee::getAge).thenComparing(Employee::getName));
        System.out.println(Arrays.equals(employeesList.toArray(), employeesSortedByAge));
        System.out.println(employeesList);
	}

}

class Employee implements Comparable {

	private String name;
	private int age;
	private double salary;

	public Employee(String name, int age, double salary) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public boolean equals(Object obj) {
		return ((Employee) obj).getName().equals(getName());
	}

	@Override
	public int compareTo(Object o) {
		Employee e = (Employee) o;
		return getName().compareTo(e.getName());
	}

	@Override
	public String toString() {
		return new StringBuffer().append("(").append(getName()).append(getAge()).append(",").append(getSalary())
				.append(")").toString();
	}

}