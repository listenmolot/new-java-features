import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorAndComparable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		comparable();
		comparatorAge();
		comparatorRanking();
		whenComparing_UsingLambda_thenSorted();
		whenComparing_UsingComparatorComparing_thenSorted();
		givenTwoPlayers_whenUsingSubtraction_thenOverflow();
	}

	static void comparable() {
		System.out.println("--comparable");
		List<Player1> footballTeam = new ArrayList<Player1>();
		Player1 player1 = new Player1(59, "John", 20);
		Player1 player2 = new Player1(67, "Roger", 22);
		Player1 player3 = new Player1(45, "Steven", 24);
		footballTeam.add(player1);
		footballTeam.add(player2);
		footballTeam.add(player3);

		System.out.println("Before Sorting : " + footballTeam);
		Collections.sort(footballTeam);
		System.out.println("After Sorting : " + footballTeam);
	}

	static void comparatorAge() {
		System.out.println("--comparatorAge");
		List<Player2> footballTeam = new ArrayList<Player2>();
		Player2 player1 = new Player2(59, "John", 22);
		Player2 player2 = new Player2(67, "Roger", 20);
		Player2 player3 = new Player2(45, "Steven", 24);
		footballTeam.add(player1);
		footballTeam.add(player2);
		footballTeam.add(player3);

		System.out.println("Before Sorting : " + footballTeam);
		// Instance of PlayerAgeComparator
		PlayerAgeComparator playerComparator = new PlayerAgeComparator();
		Collections.sort(footballTeam, playerComparator);
		System.out.println("After Sorting by age : " + footballTeam);
	}

	static void comparatorRanking() {
		System.out.println("--comparatorRanking");
		List<Player2> footballTeam = new ArrayList<Player2>();
		Player2 player1 = new Player2(59, "John", 22);
		Player2 player2 = new Player2(67, "Roger", 20);
		Player2 player3 = new Player2(45, "Steven", 40);
		footballTeam.add(player1);
		footballTeam.add(player2);
		footballTeam.add(player3);

		System.out.println("Before Sorting : " + footballTeam);
		// Instance of PlayerRankingComparator
		PlayerRankingComparator playerComparator = new PlayerRankingComparator();
		Collections.sort(footballTeam, playerComparator);
		System.out.println("After Sorting by ranking : " + footballTeam);
	}

	static void whenComparing_UsingLambda_thenSorted() {
		System.out.println("--whenComparing_UsingLambda_thenSorted");
		List<Player2> footballTeam = new ArrayList<Player2>();
		Player2 player1 = new Player2(59, "John", 20);
		Player2 player2 = new Player2(67, "Roger", 22);
		Player2 player3 = new Player2(45, "Steven", 24);
		footballTeam.add(player1);
		footballTeam.add(player2);
		footballTeam.add(player3);
		
		Comparator<Player2> byRanking = (Player2 p1, Player2 p2) -> Integer.compare(p1.getRanking(), p2.getRanking());

		System.out.println("Before Sorting : " + footballTeam);
		Collections.sort(footballTeam, byRanking);
		System.out.println("After Sorting : " + footballTeam);
		System.out.println(footballTeam.get(0).getName());
		System.out.println(footballTeam.get(2).getRanking());
	}

	static void whenComparing_UsingComparatorComparing_thenSorted() {
		System.out.println("--whenComparing_UsingLambda_thenSorted");
		List<Player2> footballTeam = new ArrayList<Player2>();
		Player2 player1 = new Player2(59, "John", 20);
		Player2 player2 = new Player2(67, "Roger", 22);
		Player2 player3 = new Player2(45, "Steven", 24);
		footballTeam.add(player1);
		footballTeam.add(player2);
		footballTeam.add(player3);
		
		System.out.println("********* byRanking *********");
		Comparator<Player2> byRanking = Comparator.comparing(Player2::getRanking);

		System.out.println("Before Sorting : " + footballTeam);
		Collections.sort(footballTeam, byRanking);
		System.out.println("After Sorting : " + footballTeam);
		System.out.println(footballTeam.get(0).getName());
		System.out.println(footballTeam.get(2).getRanking());

		System.out.println("********* byAge *********");
		Comparator<Player2> byAge = Comparator.comparing(Player2::getAge);

		System.out.println("Before Sorting : " + footballTeam);
		Collections.sort(footballTeam, byAge);
		System.out.println("After Sorting : " + footballTeam);
		System.out.println(footballTeam.get(0).getName());
		System.out.println(footballTeam.get(2).getRanking());
	}
	
	static void givenTwoPlayers_whenUsingSubtraction_thenOverflow() {
		System.out.println("--givenTwoPlayers_whenUsingSubtraction_thenOverflow");
        Comparator<Player2> comparator = (p1, p2) -> p1.getRanking() - p2.getRanking();
        Player2 player1 = new Player2(59, "John", Integer.MAX_VALUE);
        Player2 player2 = new Player2(67, "Roger", -1);

        List<Player2> players = Arrays.asList(player1, player2);
        players.sort(comparator);
        System.out.println(players);

        System.out.println(players.get(0).getName());
        System.out.println(players.get(1).getName());
    }
	

}

class PlayerAgeComparator implements Comparator<Player2> {

	@Override
	public int compare(Player2 firstPlayer, Player2 secondPlayer) {
		return Integer.compare(firstPlayer.getAge(), secondPlayer.getAge());
	}

}

class PlayerRankingComparator implements Comparator<Player2> {

	@Override
	public int compare(Player2 firstPlayer, Player2 secondPlayer) {
		return Integer.compare(firstPlayer.getRanking(), secondPlayer.getRanking());
	}

}

class Player1 implements Comparable<Player1> {

	private int ranking;

	private String name;

	private int age;

	public Player1(int ranking, String name, int age) {
		this.ranking = ranking;
		this.name = name;
		this.age = age;
	}

	public int getRanking() {
		return ranking;
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return this.name;
	}

	@Override
	public int compareTo(Player1 otherPlayer) {
		return Integer.compare(getRanking(), otherPlayer.getRanking());
	}

}

class Player2 {

	private int ranking;

	private String name;

	private int age;

	public Player2(int ranking, String name, int age) {
		this.ranking = ranking;
		this.name = name;
		this.age = age;
	}

	public int getRanking() {
		return ranking;
	}

	public void setRanking(int ranking) {
		this.ranking = ranking;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return this.name;
	}

}