import java.util.Arrays;
import java.util.List;

import static java.util.Comparator.*;


//http://blog.marcinchwedczuk.pl/comparing-with-nullsFirst-and-nullsLast

public class SortingWithComparators {

	private final String value;

	public SortingWithComparators(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	@Override
	public String toString() {
		return String.format("SortingWithComparators(%s)", this.value);
	}

	public static void main(String[] args) {
		withoutNulls();
		try {
			withNullsException();
		} catch (Exception e) {
			System.out.println("withNullsException() -> java.lang.NullPointerException");
		}
		withNullsNoException();
		try {
			withNullsPropertyException();
		} catch (Exception e) {
			System.out.println("withNullsPropertyException() -> java.lang.NullPointerException");
		}
		withNullsPropertyNoException();
		System.out.println("ss");
	}

	static void withoutNulls() {
		System.out.println("--withoutNulls");
		List<SortingWithComparators> listOfData = Arrays.asList(new SortingWithComparators("foo"),
				new SortingWithComparators("bar"), new SortingWithComparators("nyu"));

		listOfData.sort(comparing(SortingWithComparators::getValue));
		listOfData.forEach(System.out::println);
	}

	static void withNullsException() {
		System.out.println("withNullsException");
		List<SortingWithComparators> listOfData = Arrays.asList(new SortingWithComparators("foo"), null,
				new SortingWithComparators("bar"), new SortingWithComparators("nyu"));

		listOfData.sort(comparing(SortingWithComparators::getValue));
		listOfData.forEach(System.out::println);
	}

	static void withNullsNoException() {
		System.out.println("withNullsNoException");
		List<SortingWithComparators> listOfData = Arrays.asList(new SortingWithComparators("foo"), null,
				new SortingWithComparators("bar"), new SortingWithComparators("nyu"));

		listOfData.sort(nullsFirst(comparing(SortingWithComparators::getValue)));
		listOfData.forEach(System.out::println);
	}

	static void withNullsPropertyException() {
		System.out.println("withNullsPropertyException");
		List<SortingWithComparators> listOfData = Arrays.asList(new SortingWithComparators("foo"),
				new SortingWithComparators(null), new SortingWithComparators("bar"), new SortingWithComparators("nyu"));

		listOfData.sort(nullsFirst(comparing(SortingWithComparators::getValue)));
		listOfData.forEach(System.out::println);
	}

	static void withNullsPropertyNoException() {
		System.out.println("withNullsPropertyNoException");
		List<SortingWithComparators> listOfData = Arrays.asList(new SortingWithComparators("foo"),
				new SortingWithComparators(null), new SortingWithComparators("bar"), new SortingWithComparators("nyu"));

		listOfData.sort(nullsFirst(comparing(SortingWithComparators::getValue, nullsFirst(naturalOrder()))));

		listOfData.forEach(System.out::println);
	}

}
