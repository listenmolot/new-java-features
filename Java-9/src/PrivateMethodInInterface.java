
public class PrivateMethodInInterface {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Foo customFoo = new CustomFoo();
		customFoo.bar();
		Foo.buzz();
	}

}

interface Foo {
	default void bar() {
		System.out.print("Hello");
		baz();
	}

	private void baz() {
		System.out.println(" world!");
	}

	static void buzz() {
		System.out.print("Hello");
		staticBaz();
	}

	private static void staticBaz() {
		System.out.println(" static world!");
	}
}

class CustomFoo implements Foo {}
