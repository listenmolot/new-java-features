import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ConvenienceFactoryMethodsForCollections {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		before9();
		after9();
	}

	static void before9() {

		// Creating a small immutable Collection in Java is very verbose using the
		// traditional way
		Set<String> set = new HashSet<>();
		set.add("foo");
		set.add("bar");
		set.add("baz");
		set = Collections.unmodifiableSet(set);

		// the same for the map

		// However, for List, there's a factory method (ineffective because of creation
		// of array, less obvious beacuse of class Arrays)
		List<String> list = Arrays.asList("foo", "bar", "baz");

		// There are other ways of reducing verbosity like the double-brace
		// initialization technique
		// The double brace technique is only a little less verbose but greatly reduces
		// the readability (and is considered an anti-pattern).?
		Set<String> set2 = Collections.unmodifiableSet(new HashSet<String>() {
			{
				add("foo");
				add("bar");
				add("baz");
			}
		});
		System.out.println("set2 = " + set2);

		List<String> list2 = Collections.unmodifiableList(new ArrayList<String>() {
			{
				add("foo");
				add("bar");
				add("baz");
			}
		});
		System.out.println("list2 = " + list2);

		Map<String, String> map2 = Collections.unmodifiableMap(new HashMap<String, String>() {
			{
				put("foo", "foo");
				put("bar", "bar");
				put("baz", "baz");
			}
		});
		System.out.println("map2 = " + map2);

		// by using Java 8 Streams
		// The Java 8 version, though, is a one-line expression, and it has some
		// problems, too.
		// First, it's not obvious and intuitive.
		// Second, it's still verbose.
		// Third, it involves the creation of unnecessary objects.
		// And fourth, this method can't be used for creating a Map.
		Set<String> set3 = Stream.of("foo", "bar", "baz")
				.collect(Collectors.collectingAndThen(Collectors.toSet(), Collections::unmodifiableSet));
		System.out.println("set3 = " + set3);
	}

	static void after9() {
		// In the case of List and Set, no elements can be null. 
		//In the case of a Map, neither keys nor values can be null. 
		//Passing null argument throws a NullPointerException:
		List<String> list = List.of("foo", "bar", "baz");
		Set<String> set = Set.of("foo", "bar", "baz");
		Map<String, String> map = Map.of("foo", "a", "bar", "b", "baz", "c");
		
		//The collections created using factory methods are immutable, and changing an element, 
		//adding new elements, or removing an element throws UnsupportedOperationException
//		set.add("baz");
//		list.set(0, "baz");
//		map.remove("foo");

		// During the creation of a Set using a factory method, if duplicate elements
		// are passed as parameters,
		// then IllegalArgumentException is thrown at runtime
		// Set.of("foo", "bar", "baz", "foo");

		// In the case of Map, there is a different method for more than 10 key-value
		// pairs:
		Map<String, String> map2 = Map.ofEntries(new AbstractMap.SimpleEntry<>("foo", "a"),
				new AbstractMap.SimpleEntry<>("bar", "b"), new AbstractMap.SimpleEntry<>("baz", "c"));
		System.out.println("map2 = " + map2);

		// Passing in duplicate values for Key would throw an IllegalArgumentException:
		// Map.of("foo", "a", "foo", "b");

		// If an array of primitive type is passed, a List of array of that primitive
		// type is returned.
		// In this case, a List<int[]> of size 1 is returned and the element at index 0
		// contains the array.
		int[] arr = { 1, 2, 3, 4, 5 };
		List<int[]> listArray = List.of(arr);
		System.out.println("listArray = " + listArray);
		
		//The instances created by factory methods are value-based. 
		//This means that factories are free to create a new instance or return an existing instance.
		//Hence, if we create Lists with same values, they may or may not refer to the same object on the heap.
		//In this case, listA == listB may or may not evaluate to true depending on the JVM.
		List<String> listA = List.of("foo", "bar");
		List<String> listB = List.of("foo", "bar");
		System.out.println("listA == listB: "+(listA == listB));

	}

}
