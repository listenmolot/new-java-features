import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

//https://www.baeldung.com/java9-stream-collectors

public class NewStreamCollectors {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		filtering();
		flatMapping();
	}

	static void filtering() {
		System.out.println("filtering");
		List<Integer> numbers = List.of(0, 1, 1, 2, 3, 3, 3, 4, 5, 5, 5, 6, 6);

		Map<Integer, Long> result = numbers.stream().filter(val -> val > 3)
				.collect(Collectors.groupingBy(i -> i, Collectors.counting()));

		System.out.println(result.size());

		result = numbers.stream()
				.collect(Collectors.groupingBy(i -> i, Collectors.filtering(val -> val > 3, Collectors.counting())));

		System.out.println(result.size());
		System.out.println(result);

		Map<Integer, List<Integer>> result2 = numbers.stream()
				.collect(Collectors.groupingBy(i -> i, Collectors.filtering(val -> val > 3, Collectors.toList())));

		System.out.println(result2);

		/// test
		List<String> list = List.of("x", "yy", "zz", "yy", "yz", "xx", "www");

		Map<Integer, List<String>> result3 = list.stream().collect(Collectors.groupingBy(String::length,
				Collectors.filtering(s -> !s.contains("z"), Collectors.toList())));

		System.out.println(result3);
	}

	static void flatMapping() {
		System.out.println("flatMapping");
		Blog blog1 = new Blog("1", "Nice", "Very Nice");
		Blog blog2 = new Blog("2", "Disappointing", "Ok", "Could be better");
		List<Blog> blogs = List.of(blog1, blog2);

		Map<String, List<List<String>>> authorComments1 = blogs.stream().collect(
				Collectors.groupingBy(Blog::getAuthorName, Collectors.mapping(Blog::getComments, Collectors.toList())));

		System.out.println(authorComments1);

		System.out.println(authorComments1.size());
		System.out.println(authorComments1.get("1").get(0).size());
		System.out.println(authorComments1.get("2").get(0).size());

		Map<String, List<String>> authorComments2 = blogs.stream().collect(Collectors.groupingBy(Blog::getAuthorName,
				Collectors.flatMapping(blog -> blog.getComments().stream(), Collectors.toList())));

		System.out.println(authorComments2);

		System.out.println(authorComments2.size());
		System.out.println(authorComments2.get("1").size());
		System.out.println(authorComments2.get("2").size());
	}
}

class Blog {
	private String authorName;
	private List<String> comments;

	public Blog(String authorName, String... comments) {
		super();
		this.authorName = authorName;
		this.comments = List.of(comments);
	}

	public String getAuthorName() {
		return authorName;
	}

	public List<String> getComments() {
		return comments;
	}
}
