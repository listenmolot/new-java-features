import java.util.List;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


//https://www.baeldung.com/java-9-optional

public class OptionalAdds {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		or();
		ifPresentOrElse();
		stream();
	}

	static void or() {
		orEmpty();
		orNotEmpty();
	}

	static void orEmpty() {
		System.out.println("orEmpty");
		// given
		String defaultString = "default";
		Optional<String> value = Optional.empty();
		Optional<String> defaultValue = Optional.of(defaultString);

		// when
		Optional<String> result = value.or(() -> defaultValue);

		// then
		System.out.println(result.get() + " = " + defaultString);
	}

	static void orNotEmpty() {
		System.out.println("orNotEmpty");
		// given
		String expected = "properValue";
		Optional<String> value = Optional.of(expected);
		Optional<String> defaultValue = Optional.of("default");

		// when
		Optional<String> result = value.or(() -> defaultValue);

		// then
		System.out.println(result.get() + " = " + expected);
	}

	static void ifPresentOrElse() {
		ifPresentOrElseEmtpy();
		ifPresentOrElseNotEmtpy();
	}

	static void ifPresentOrElseEmtpy() {
		System.out.println("ifPresentOrElseEmtpy");
		// given
		Optional<String> value = Optional.empty();
		AtomicInteger successCounter = new AtomicInteger(0);
		AtomicInteger onEmptyOptionalCounter = new AtomicInteger(0);

		// when
		value.ifPresentOrElse(v -> successCounter.incrementAndGet(), onEmptyOptionalCounter::incrementAndGet);

		// then
		System.out.println(successCounter.get() == 0);
		System.out.println(onEmptyOptionalCounter.get() == 1);
	}

	static void ifPresentOrElseNotEmtpy() {
		System.out.println("ifPresentOrElseNotEmtpy");
		// given
		Optional<String> value = Optional.of("properValue");
		AtomicInteger successCounter = new AtomicInteger(0);
		AtomicInteger onEmptyOptionalCounter = new AtomicInteger(0);

		// when
		value.ifPresentOrElse(v -> successCounter.incrementAndGet(), onEmptyOptionalCounter::incrementAndGet);

		// then
		System.out.println(successCounter.get() == 1);
		System.out.println(onEmptyOptionalCounter.get() == 0);
	}

	static void stream() {
		streamEmpty();
		streamNotEmpty();
	}

	static void streamEmpty() {
		// given
	    Optional<String> value = Optional.empty();

	    // when
	    List<String> collect = value.stream().map(String::toUpperCase).collect(Collectors.toList());

	    // then
	    System.out.println(collect);
	}

	static void streamNotEmpty() {
		// given
	    Optional<String> value = Optional.of("a");

	    // when
	    List<String> collect = value.stream().map(String::toUpperCase).collect(Collectors.toList());

	    // then
	    System.out.println(collect);
	}

}
