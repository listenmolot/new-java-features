import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FilteringStreamOfOptionals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Optional<String>> listOfOptionals = Arrays.asList(
				  Optional.empty(), Optional.of("foo"), Optional.empty(), Optional.of("bar"));
		
		System.out.println(listOfOptionals);
		
		java8Filter(listOfOptionals);
		java8FlatMap(listOfOptionals);
		java9Optional__stream(listOfOptionals);
	}

	static void java8Filter(List<Optional<String>> listOfOptionals) {
		System.out.println("java8Filter");
		List<String> filteredList = listOfOptionals.stream()
				  .filter(Optional::isPresent)
				  .map(Optional::get)
				  .collect(Collectors.toList());
		System.out.println(filteredList);
	}

	static void java8FlatMap(List<Optional<String>> listOfOptionals) {
		System.out.println("java8FlatMap");
		List<String> filteredList1 = listOfOptionals.stream()
				  .flatMap(o -> o.isPresent() ? Stream.of(o.get()) : Stream.empty())
				  .collect(Collectors.toList());
		System.out.println(filteredList1);
		
		List<String> filteredList2 = listOfOptionals.stream()
				  .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
				  .collect(Collectors.toList());
		System.out.println(filteredList2);
	}

	static void java9Optional__stream(List<Optional<String>> listOfOptionals) {
		System.out.println("java9Optional__stream");
		List<String> filteredList = listOfOptionals.stream()
				  .flatMap(Optional::stream)
				  .collect(Collectors.toList());
		System.out.println(filteredList);
	}

}
