package streams;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

//https://www.baeldung.com/java-9-objects-new

public class ObjectsAdds {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		requireNonNullElse();
		equireNonNullElseGet();
		checkIndex();
		checkFromToIndex();
		checkFromIndexSize();
	}

	static void requireNonNullElse() {

		// returns the first parameter if it is not null
		List<String> aList2 = Objects.<List<String>>requireNonNullElse(List.of("item1", "item2"), Collections.emptyList());
		System.out.println(aList2);

		// returns the second parameter if the first one is null
		List<String> aList = Objects.<List<String>>requireNonNullElse(null, Collections.emptyList());
		System.out.println(aList);

		// throws java.lang.NullPointerException if both are null
		// Objects.<List<?>>requireNonNullElse(null, null);
	}
	
	static void equireNonNullElseGet() {
		List<String> aList = Objects.<List<String>>requireNonNullElseGet(
			      null, List::of);
		System.out.println(aList);
	}
	
	static void checkIndex() {
		int length = 5;
		System.out.println(Objects.checkIndex(4, length));
		
		//throws IndexOutOfBoundsException
	    //Objects.checkIndex(5, length);
	}
	
	static void checkFromToIndex() {
		int length = 6;
		System.out.println(Objects.checkFromToIndex(2,length,length));
		
		//throws IndexOutOfBoundsException
		//Objects.checkFromToIndex(2,7,length);
	}

	static void checkFromIndexSize() {
		int length = 6;
		System.out.println(Objects.checkFromIndexSize(2,3,length));
		
		//throws IndexOutOfBoundsException
		Objects.checkFromIndexSize(2, 6, length);
	}
	
}
