import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamAPIImprovements {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		takeWhileDropWhile();
		iterate();
		ofNullable();
	}
	
	static void takeWhileDropWhile() {
		Supplier<Stream<String>> supplier = () -> Stream.iterate("", s -> s + "s")
				  .takeWhile(s -> s.length() < 10);
		
		System.out.println(supplier.get().collect(Collectors.toList()));
				
		Stream<String> ss = supplier.get().dropWhile(s -> !s.contains("sssss"));
		
		System.out.println(ss.collect(Collectors.toList()));
		
		//// or just
		
		System.out.println(Stream.iterate("", s -> s + "s")
				.takeWhile(s -> s.length() < 10)
				.dropWhile(s -> !s.contains("sssss"))
				.collect(Collectors.toList()));
	}
	
	static void iterate() {
		Stream.iterate(0, i -> i < 10, i -> i + 1)
		  .forEach(System.out::println);
		
//		It can be associated with the corresponding for statement
//		for (int i = 0; i < 10; ++i) {
//		    System.out.println(i);
//		}
	}
	
	static void  ofNullable() {
		
		List<String> collection = Arrays.asList("A", "B", "C");
        Map<String, Integer> map = new HashMap<>() {
            {
                put("A", 10);
                put("C", 30);
            }
        };
		
		//before 9
		List<Integer> list1 = collection.stream()
		  .flatMap(s -> {
		      Integer temp = map.get(s);
		      return temp != null ? Stream.of(temp) : Stream.empty();
		  })
		  .collect(Collectors.toList());
		System.out.println(list1);
		
		//since 9
		List<Integer> list2 = collection.stream()
		  .flatMap(s -> Stream.ofNullable(map.get(s)))
		  .collect(Collectors.toList());
		System.out.println(list2);
	}

}
