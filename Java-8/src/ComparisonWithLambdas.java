import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

//https://www.baeldung.com/java-8-sort-lambda

public class ComparisonWithLambdas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		givenPreLambda_whenSortingEntitiesByName_thenCorrectlySorted();
		whenSortingEntitiesByName_thenCorrectlySorted();
		givenLambdaShortForm_whenSortingEntitiesByName_thenCorrectlySorted();
		givenMethodDefinition_whenSortingEntitiesByNameThenAge_thenCorrectlySorted();
		givenInstanceMethod_whenSortingEntitiesByName_thenCorrectlySorted();
		whenSortingEntitiesByNameThenAge_thenCorrectlySorted();
		givenComposition_whenSortingEntitiesByNameThenAge_thenCorrectlySorted();
		givenStreamNaturalOrdering_whenSortingEntitiesByName_thenCorrectlySorted();
		givenStreamCustomOrdering_whenSortingEntitiesByName_thenCorrectlySorted();
		givenStreamNaturalOrdering_whenSortingEntitiesByNameReversed_thenCorrectlySorted();
		givenStreamCustomOrdering_whenSortingEntitiesByNameReversed_thenCorrectlySorted();
		givenStreamComparatorOrdering_whenSortingEntitiesByNameReversed_thenCorrectlySorted();
		try {
			givenANullElement_whenSortingEntitiesByName_thenThrowsNPE();
		} catch (Exception e) {
		}
		givenANullElement_whenSortingEntitiesByNameManually_thenMovesTheNullToLast();
		givenANullElement_whenSortingEntitiesByName_thenMovesTheNullToLast();
		givenANullElement_whenSortingEntitiesByName_thenMovesTheNullToStart();
	}

	static void givenPreLambda_whenSortingEntitiesByName_thenCorrectlySorted() {

		final List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));

		Collections.sort(humans, new Comparator<Human>() {
			@Override
			public final int compare(final Human h1, final Human h2) {
				return h1.getName().compareTo(h2.getName());
			}
		});

		System.out.println(humans.get(0).equals((new Human("Jack", 12))));
	}

	static void whenSortingEntitiesByName_thenCorrectlySorted() {
		final List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));

		humans.sort((final Human h1, final Human h2) -> h1.getName().compareTo(h2.getName()));

		System.out.println(humans.get(0).equals((new Human("Jack", 12))));
	}

	static void givenLambdaShortForm_whenSortingEntitiesByName_thenCorrectlySorted() {
		final List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));

		humans.sort((h1, h2) -> h1.getName().compareTo(h2.getName()));

		System.out.println(humans.get(0).equals((new Human("Jack", 12))));
	}

	static void givenMethodDefinition_whenSortingEntitiesByNameThenAge_thenCorrectlySorted() {
		final List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));

		humans.sort(Human::compareByNameThenAge);
		System.out.println(humans.get(0).equals((new Human("Jack", 12))));
	}

	static void givenInstanceMethod_whenSortingEntitiesByName_thenCorrectlySorted() {
		final List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));

		humans.sort(Comparator.comparing(Human::getName));
		System.out.println(humans.get(0).equals((new Human("Jack", 12))));
	}

	static void whenSortingEntitiesByNameReversed_thenCorrectlySorted() {
		final List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));
		final Comparator<Human> comparator = (h1, h2) -> h1.getName().compareTo(h2.getName());

		humans.sort(comparator.reversed());
		System.out.println(humans.get(0).equals((new Human("Sarah", 10))));
	}

	static void whenSortingEntitiesByNameThenAge_thenCorrectlySorted() {
		final List<Human> humans = new ArrayList<>(
				List.of(new Human("Sarah", 12), new Human("Sarah", 10), new Human("Zack", 12)));
		humans.sort((lhs, rhs) -> {
			if (lhs.getName().equals(rhs.getName())) {
				return Integer.compare(lhs.getAge(), rhs.getAge());
			} else {
				return lhs.getName().compareTo(rhs.getName());
			}
		});
		System.out.println(humans.get(0).equals((new Human("Sarah", 10))));
	}

	static void givenComposition_whenSortingEntitiesByNameThenAge_thenCorrectlySorted() {
		final List<Human> humans = new ArrayList<>(
				List.of(new Human("Sarah", 12), new Human("Sarah", 10), new Human("Zack", 12)));

		humans.sort(Comparator.comparing(Human::getName).thenComparing(Human::getAge));
		System.out.println(humans.get(0).equals((new Human("Sarah", 10))));
	}

	static void givenStreamNaturalOrdering_whenSortingEntitiesByName_thenCorrectlySorted() {
		List<String> letters = new ArrayList<>(List.of("B", "A", "C"));

		List<String> sortedLetters = letters.stream().sorted().collect(Collectors.toList());
		System.out.println(sortedLetters.get(0).equals(("A")));
	}

	static void givenStreamCustomOrdering_whenSortingEntitiesByName_thenCorrectlySorted() {
		List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));
		Comparator<Human> nameComparator = (h1, h2) -> h1.getName().compareTo(h2.getName());

		List<Human> sortedHumans = humans.stream().sorted(nameComparator).collect(Collectors.toList());
		System.out.println(sortedHumans.get(0).equals((new Human("Jack", 12))));
	}

	static void givenStreamComparatorOrdering_whenSortingEntitiesByName_thenCorrectlySorted() {
		List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));

		List<Human> sortedHumans = humans.stream().sorted(Comparator.comparing(Human::getName))
				.collect(Collectors.toList());

		System.out.println(sortedHumans.get(0).equals((new Human("Jack", 12))));
	}

	static void givenStreamNaturalOrdering_whenSortingEntitiesByNameReversed_thenCorrectlySorted() {
		List<String> letters = new ArrayList<>(List.of("B", "A", "C"));

		List<String> reverseSortedLetters = letters.stream().sorted(Comparator.reverseOrder())
				.collect(Collectors.toList());

		System.out.println(reverseSortedLetters.get(0).equals(("C")));
	}

	static void givenStreamCustomOrdering_whenSortingEntitiesByNameReversed_thenCorrectlySorted() {
		List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));
		Comparator<Human> reverseNameComparator = (h1, h2) -> h2.getName().compareTo(h1.getName());

		List<Human> reverseSortedHumans = humans.stream().sorted(reverseNameComparator).collect(Collectors.toList());
		System.out.println(reverseSortedHumans.get(0).equals((new Human("Sarah", 10))));
	}

	static void givenStreamComparatorOrdering_whenSortingEntitiesByNameReversed_thenCorrectlySorted() {
		List<Human> humans = new ArrayList<>(List.of(new Human("Sarah", 10), new Human("Jack", 12)));

		List<Human> reverseSortedHumans = humans.stream()
				.sorted(Comparator.comparing(Human::getName, Comparator.reverseOrder())).collect(Collectors.toList());

		System.out.println(reverseSortedHumans.get(0).equals((new Human("Sarah", 10))));
	}

	static void givenANullElement_whenSortingEntitiesByName_thenThrowsNPE() {
		List<Human> humans = new ArrayList<>(List.of(null, new Human("Jack", 12)));

		humans.sort((h1, h2) -> h1.getName().compareTo(h2.getName()));
	}

	static void givenANullElement_whenSortingEntitiesByNameManually_thenMovesTheNullToLast() {
		List<Human> humans = new ArrayList<>();
		humans.add(null);
		humans.add(new Human("Jack", 12));
		humans.add(null);

		humans.sort((h1, h2) -> {
			if (h1 == null) {
				return h2 == null ? 0 : 1;
			} else if (h2 == null) {
				return -1;
			}
			return h1.getName().compareTo(h2.getName());
		});

		System.out.println(humans.get(0));
		System.out.println(humans.get(1));
		System.out.println(humans.get(2));
	}

	static void givenANullElement_whenSortingEntitiesByName_thenMovesTheNullToLast() {
		List<Human> humans = new ArrayList<>();
		humans.add(null);
		humans.add(new Human("Jack", 12));
		humans.add(null);

		humans.sort(Comparator.nullsLast(Comparator.comparing(Human::getName)));

		System.out.println(humans.get(0));
		System.out.println(humans.get(1));
		System.out.println(humans.get(2));
	}

	static void givenANullElement_whenSortingEntitiesByName_thenMovesTheNullToStart() {
		List<Human> humans = new ArrayList<>();
		humans.add(null);
		humans.add(new Human("Jack", 12));
		humans.add(null);

		humans.sort(Comparator.nullsFirst(Comparator.comparing(Human::getName)));

		System.out.println(humans.get(0));
		System.out.println(humans.get(1));
		System.out.println(humans.get(2));
	}
}

class Human {
	private String name;
	private int age;

	public Human() {
		super();
	}

	public Human(final String name, final int age) {
		super();

		this.name = name;
		this.age = age;
	}

	// API

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(final int age) {
		this.age = age;
	}

	// compare

	public static int compareByNameThenAge(final Human lhs, final Human rhs) {
		if (lhs.name.equals(rhs.name)) {
			return Integer.compare(lhs.age, rhs.age);
		} else {
			return lhs.name.compareTo(rhs.name);
		}
	}

	//

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + age;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Human other = (Human) obj;
		if (age != other.age) {
			return false;
		}
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		final StringBuilder builder = new StringBuilder();
		builder.append("Human [name=").append(name).append(", age=").append(age).append("]");
		return builder.toString();
	}

}