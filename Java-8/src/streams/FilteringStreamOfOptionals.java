package streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

//https://www.baeldung.com/java-filter-stream-of-optional#Introduction

public class FilteringStreamOfOptionals {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<Optional<String>> listOfOptionals = Arrays.asList(
				  Optional.empty(), Optional.of("foo"), Optional.empty(), Optional.of("bar"));
		
		byFilter(listOfOptionals);
		byFlatMap(listOfOptionals);
		byOptionalStreamJava9(listOfOptionals);
	}
	
	static void byFilter(List<Optional<String>> listOfOptionals) {
		List<String> filteredList = listOfOptionals.stream()
				  .filter(Optional::isPresent)
				  .map(Optional::get)
				  .collect(Collectors.toList());
		System.out.println(filteredList);
	}
	
	static void byFlatMap(List<Optional<String>> listOfOptionals) {
		List<String> filteredList1 = listOfOptionals.stream()
				  .flatMap(o -> o.isPresent() ? Stream.of(o.get()) : Stream.empty())
				  .collect(Collectors.toList());
		System.out.println(filteredList1);
		
		List<String> filteredList2 = listOfOptionals.stream()
				  .flatMap(o -> o.map(Stream::of).orElseGet(Stream::empty))
				  .collect(Collectors.toList());
		System.out.println(filteredList2);
	}
	
	static void byOptionalStreamJava9(List<Optional<String>> listOfOptionals) {
		List<String> filteredList = listOfOptionals.stream()
				  .flatMap(Optional::stream)
				  .collect(Collectors.toList());
		System.out.println(filteredList);
	}
	
	

}
