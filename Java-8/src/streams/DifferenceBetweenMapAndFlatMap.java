package streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DifferenceBetweenMapAndFlatMap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		inOptional();
		inStream();
		flatteningNestedCollections();
	}
	
	static void inOptional() {
		System.out.println("Optional.of(\"TEST\").equals(Optional.of(\"test\").map(String::toUpperCase)): "+
				Optional.of("TEST").equals(Optional.of("test").map(String::toUpperCase)));
		System.out.println(Optional.of("TEST"));
		System.out.println(Optional.of("test").map(String::toUpperCase));
		
		System.out.println("Optional.of(Optional.of(\"STRING\")).equals(Optional.of(\"string\").map(s2 -> Optional.of(\"STRING\"))): "+
				Optional.of(Optional.of("STRING")).equals(Optional.of("string").map(s2 -> Optional.of("STRING"))));
		System.out.println(Optional.of(Optional.of("STRING")));
		System.out.println(Optional.of("string").map(s2 -> Optional.of("STRING")));
		
		System.out.println("Optional.of(\"STRING\").equals(Optional.of(\"string\").flatMap(s3 -> Optional.of(\"STRING\"))): " +
		Optional.of("STRING").equals(Optional.of("string").flatMap(s3 -> Optional.of("STRING"))));
		System.out.println(Optional.of("STRING"));
		System.out.println(Optional.of("string").flatMap(s3 -> Optional.of("STRING")));
	}
	
	static void inStream() {
		List<String> myList = Stream.of("a", "b")
				  .map(String::toUpperCase)
				  .collect(Collectors.toList());
		System.out.println("Arrays.asList(\"A\", \"B\").equals(myList): "+Arrays.asList("A", "B").equals(myList));
		
		List<List<String>> list = Arrays.asList(
				  Arrays.asList("a", "b"),
				  Arrays.asList("c", "d"));
		System.out.println(list);

		
		System.out.println(list
				  .stream()
				  .flatMap(Collection::stream)
				  .collect(Collectors.toList()));
	}
	
	static void flatteningNestedCollections() {
		List<List<String>> nestedList = Arrays.asList(
				  Arrays.asList("one:one"), 
				  Arrays.asList("two:one", "two:two", "two:three"), 
				  Arrays.asList("three:one", "three:two", "three:three", "three:four"));
		
		System.out.println(nestedList);
		
		System.out.println(flattenListOfListsImperatively(nestedList));
		
		System.out.println(flattenListOfListsStream(nestedList));
	}
	
	static <T> List<T> flattenListOfListsImperatively(List<List<T>> nestedList) {
	    List<T> ls = new ArrayList<>();
	    nestedList.forEach(ls::addAll);
	    return ls;
	}
	
	static <T> List<T> flattenListOfListsStream(List<List<T>> list) {
	    return list.stream()
	      .flatMap(Collection::stream)
	      .collect(Collectors.toList());    
	}

}
