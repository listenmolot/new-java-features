import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class VarForLambda {

	public static void main(String...strings ) {
		List<String> sampleList = Arrays.asList("Java", "Kotlin");
		String resultString = sampleList.stream()
		  .map((@Nonnull var x) -> x.toUpperCase())
		  .collect(Collectors.joining(", "));
		System.out.println(resultString);
	}
}
