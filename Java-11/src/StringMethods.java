import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class StringMethods {

	public static void main(String[] args) {
		String multilineString = "Baeldung helps \n \n developers \n explore Java.";
		List<String> lines = multilineString.lines()
		  .filter(line -> !line.isBlank())
		  .map(String::strip)
		  .collect(Collectors.toList());
		System.out.println(lines);
		
		multilineString.lines().forEach(action->System.out.println(action));
		multilineString.lines().filter(line -> !line.isBlank()).forEach(action->System.out.println(action));
		repeat();
		strip();
		isBlank();
		lines();
		newFilesMethods();
		newToArrayMethod();
	}
	
	static void repeat() {
		String output = "La ".repeat(3) + "Land";
		
		System.out.println("--repeat");
	    System.out.println("output = "+output);
	}

	static void strip() {
		String strip = "\n\t  hello   \u2005".strip();
		System.out.println("--strip");
	    System.out.println("strip = "+strip);
	    
	    String stripLeading = "\n\t  hello   \u2005".stripLeading();
		System.out.println("--stripLeading");
	    System.out.println("stripLeading = "+stripLeading);
	    
	    String stripTrailing = "\n\t  hello   \u2005".stripTrailing();
		System.out.println("--stripTrailing");
	    System.out.println("stripTrailing = "+stripTrailing);
	    
	    //trim
	    System.out.println("trim = " + "\n\t  hello   \u2005".trim());
	}
	
	static void isBlank() {
		System.out.println("isBlank = " + "\n\t\u2005  ".isBlank());
	}
	
	static void lines() {
		String multilineStr = "This is\n \n \r \r\n a multiline\n string.";

	    long lineCount = multilineStr.lines()
	      .filter(String::isBlank)
	      .count();

	    System.out.println("count = " + lineCount);
	    
	    
	    multilineStr = "This is\n\n\n\n a multiline\n string.";

	    lineCount = multilineStr.lines()
	      .filter(String::isEmpty)
	      .count();

	    System.out.println("count = " + lineCount);
	}
	
	static void newFilesMethods() {
//		Path filePath = Files.writeString(Files.createTempFile(tempDir, "demo", ".txt"), "Sample text");
//		String fileContent = Files.readString(filePath);
//		assertThat(fileContent).isEqualTo("Sample text");
	}
	
	static void newToArrayMethod() {
		List<String> sampleList = Arrays.asList("Java", "Kotlin");
		String[] sampleArray = sampleList.toArray(String[]::new);
		System.out.println(Arrays.asList(sampleArray));
	}
	
	static void not(){
		List<String> sampleList = Arrays.asList("Java", "\n \n", "Kotlin", " ");
		List withoutBlanks = sampleList.stream()
		  .filter(Predicate.not(String::isBlank))
		  .collect(Collectors.toList());
		System.out.println(withoutBlanks);	
	}
	
}
