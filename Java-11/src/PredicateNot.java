import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateNot {
	public static void main(String...strings) {
		before11OnlyAdults();
		before11NotAdults();
		after11OnlyAdults();
	}
	
	static void before11OnlyAdults() {
		List<Person> people = Arrays.asList(
				  new Person(1),
				  new Person(18),
				  new Person(2)
				);
		List adults = people.stream().filter(Person::isAdult).collect(Collectors.toList());
		System.out.println(adults);
	}
	
	static void before11NotAdults() {
		List<Person> people = Arrays.asList(
				  new Person(1),
				  new Person(18),
				  new Person(2)
				);
		List adults1 = people.stream().filter(person -> !person.isAdult()).collect(Collectors.toList());
		List adults2 = people.stream().filter(Person::isNotAdult).collect(Collectors.toList());
		System.out.println(adults1);
		System.out.println(adults2);
	}
	
	static void after11OnlyAdults() {
		List<Person> people = Arrays.asList(
				  new Person(1),
				  new Person(18),
				  new Person(2)
				);
		List adults = people.stream().filter(Predicate.not(Person::isAdult)).collect(Collectors.toList());
		System.out.println(adults);
	}
}

class Person {
    private static final int ADULT_AGE = 18;

    private int age;

    public Person(int age) {
        this.age = age;
    }

    public boolean isAdult() {
        return age >= ADULT_AGE;
    }
    
    public boolean isNotAdult() {
        return age < ADULT_AGE;
    }
    
    @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	return age+"";
    }
}